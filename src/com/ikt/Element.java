package com.ikt;

public class Element {

    int value;
    Element successor;

    public Element(int value) {
        this.value = value;
        this.successor = null;
    }

    public int getValue() {
        return this.value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Element getSuccessor() {
        return this.successor;
    }

    public void setSuccessor(Element successor) {
        this.successor = successor;
    }

    public boolean hasNext() { return this.getSuccessor() != null; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Element element = (Element) o;
        return this.value == element.value;
    }
}

